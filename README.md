# Logfly

Logfly est un logiciel gratuit open source de gestion de carnet de vols parapente.

Le site du developpeur :

[https://www.logfly.org/doku.php?id=start](https://www.logfly.org/doku.php?id=start)

Il permet de stocker, visualiser et évaluer les traces de vols enregistrées avec un GPS.
Ce programme fonctionne sous Windows, macOS et Linux. Consulter la page Téléchargement pour installer le programme.

Suite à ce projet j'ai amené ma contribution en créant un paquet RPM pour les distributions **RedHat Like**. Il peut également s'installer sur les distribution **Debian Like** en installant `rpm`

```bash
    # apt install rpm
```
---
**NOTE**

Le paquet peut se télécharger ici :

---


[https://gitlab.com/patricklbs/logfly5/-/raw/master/package/Logfly-5-1.fc35.noarch.rpm ](https://gitlab.com/patricklbs/logfly5/-/raw/master/package/Logfly-5-1.fc35.noarch.rpm)

```bash
    # wget https://gitlab.com/patricklbs/logfly5/-/raw/master/package/Logfly-5-1.fc35.noarch.rpm
    # rpm -ivh Logfly-5-1.fc35.noarch.rpm
    # dnf localinstall Logfly-5-1.fc35.noarch.rpm
    # yum localinstall https://gitlab.com/patricklbs/logfly5/-/raw/master/package/Logfly-5-1.fc35.noarch.rpm
```






