Name:           Logfly
Version:        5
Release:        1%{?dist}
Summary:        Logfly est un logiciel gratuit open source de gestion de carnet de vols parapente. 

Packager: 	Patrick Le Bas <plbs@free.fr>
Vendor: 	giloutho


License:        GPL
URL:            https://www.logfly.org
Source0:	https://gitlab.com/patricklbs/logfly5/-/raw/develop/sources/Logfly5.tar.gz
Source1:	logfly5.desktop
Source2:	Logfly5.png
Source3:	50-flymasterusb.rules
Requires:	gtk2
BuildRoot:  	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch


Provides:	libattach.so()(64bit), libattach.so(SUNWprivate_1.1)(64bit), libavplugin-53.so()(64bit), libavplugin-54.so()(64bit), libavplugin-55.so()(64bit), libavplugin-56.so()(64bit), libavplugin-ffmpeg-56.so()(64bit), libawt.so()(64bit),  libawt_headless.so()(64bit), libawt_headless.so(SUNWprivate_1.1)(64bit), libawt_xawt.so()(64bit), libawt_xawt.so(SUNWprivate_1.1)(64bit), libbci.so()(64bit), libbci.so(SUNWprivate_1.1)(64bit), libdcpr.so()(64bit), libdcpr.so(SUNWprivate_1.1)(64bit), libdecora_sse.so()(64bit), libdeploy.so()(64bit), libdt_socket.so()(64bit), libdt_socket.so(SUNWprivate_1.1)(64bit), libfontmanager.so()(64bit), libfontmanager.so(SUNWprivate_1.1)(64bit), libfxplugins.so()(64bit), libglass.so()(64bit),  libgstreamer-lite.so()(64bit), libhprof.so()(64bit), libhprof.so(SUNWprivate_1.1)(64bit), libinstrument.so()(64bit), libinstrument.so(SUNWprivate_1.1)(64bit), libj2gss.so()(64bit), libj2gss.so(SUNWprivate_1.1)(64bit), libj2pcsc.so()(64bit), libj2pcsc.so(SUNWprivate_1.1)(64bit), libj2pkcs11.so()(64bit), libj2pkcs11.so(SUNWprivate_1.1)(64bit), libjaas_unix.so()(64bit), libjava.so()(64bit), libjava.so(SUNWprivate_1.1)(64bit), libjava_crw_demo.so()(64bit), libjava_crw_demo.so(SUNWprivate_1.1)(64bit), libjavafx_font.so()(64bit), libjavafx_font_freetype.so()(64bit), libjavafx_font_pango.so()(64bit), libjavafx_font_t2k.so()(64bit), libjavafx_iio.so()(64bit), libjawt.so()(64bit), libjawt.so(SUNWprivate_1.1)(64bit), libjdwp.so()(64bit), libjdwp.so(SUNWprivate_1.1)(64bit), libjfr.so()(64bit), libjfr.so(SUNWprivate_1.1)(64bit), libjfxmedia.so()(64bit), libjfxwebkit.so()(64bit), libjfxwebkit.so(SUNWprivate_1.0)(64bit), libjli.so()(64bit), libjli.so(SUNWprivate_1.1)(64bit), libjpeg.so()(64bit), libjpeg.so(SUNWprivate_1.1)(64bit), libjsdt.so()(64bit), libjsdt.so(SUNWprivate_1.1)(64bit), libjsig.so()(64bit), libjsound.so()(64bit), libjsound.so(SUNWprivate_1.1)(64bit), libjsoundalsa.so()(64bit), libjsoundalsa.so(SUNWprivate_1.1)(64bit), libjvm.so()(64bit), libjvm.so(SUNWprivate_1.1)(64bit), libkcms.so()(64bit), libkcms.so(SUNWprivate_1.1)(64bit), liblcms.so()(64bit), liblcms.so(SUNWprivate_1.1)(64bit), libmanagement.so()(64bit), libmanagement.so(SUNWprivate_1.1)(64bit), libmlib_image.so()(64bit), libmlib_image.so(SUNWprivate_1.1)(64bit), libnet.so()(64bit), libnet.so(SUNWprivate_1.1)(64bit), libnio.so()(64bit), libnio.so(SUNWprivate_1.1)(64bit), libnpt.so()(64bit), libnpt.so(SUNWprivate_1.1)(64bit), libpackager.so()(64bit), libprism_common.so()(64bit), libprism_es2.so()(64bit), libprism_sw.so()(64bit), libresource.so()(64bit), libresource.so(SUNWprivate_1.1)(64bit), libsaproc.so()(64bit), libsaproc.so(SUNWprivate_1.1)(64bit), libsctp.so()(64bit), libsctp.so(SUNWprivate_1.1)(64bit), libsplashscreen.so()(64bit), libsplashscreen.so(SUNWprivate_1.1)(64bit), libsunec.so()(64bit), libsunec.so(SUNWprivate_1.1)(64bit), libt2k.so()(64bit), libt2k.so(SUNWprivate_1.1)(64bit), libunpack.so()(64bit), libunpack.so(SUNWprivate_1.1)(64bit), libverify.so()(64bit), libverify.so(SUNWprivate_1.1)(64bit), libzip.so()(64bit), libzip.so(SUNWprivate_1.1)(64bit)



#==============================================================================
# debug par rapport aux librairies
#==============================================================================
AutoReqProv: no

%define debug_package %{nil}
%define __jar_repack %{nil}
%define _binaries_in_noarch_packages_terminate_build   0

#==============================================================================
# End of debug
#==============================================================================



#==============================================================================
# Variables
#==============================================================================


%global logfly_dir /opt/Logfly5
%global logfly_icon /usr/share/icons
%global logfly_app /usr/share/applications
%global logfly_app_lib /opt/Logfly/app/lib
%global logfly_run_lib /opt/Logfly/runtime/lib
%global udev_dir /etc/udev/rules.d

%global _File1 logfly5.desktop
%global _File2 Logfly5.png
%global _File3 50-flymasterusb.rules

%global __requires_exclude_from ^%{logfly_app}/runtime/lib$
%global __requires_exclude_from ^%{logfly_app}/app/lib$
%global __provides_exclude ^libavcodec\\.so.*$
%global __provides_exclude ^libavformat\\.so.*$
%global __provides_exclude ^libavformat-ffmpeg\\.so.*$

%global to_exclude ^libavformat\\.so.*$
%global to_exclude ^libavcodec\\.so.*$
%global to_exclude ^libavformat-ffmpeg\\.so.*$


%description
Logfly est un logiciel gratuit open source de gestion de carnet de vols parapente.
Il permet de stocker, visualiser et évaluer les traces de vols enregistrées avec un GPS. 


%prep
%setup -q -n %{name}%{version} 


%build

#==============================================================================
# Install
#==============================================================================

%install
%{__rm} -rf %{buildroot}

install -d -m 755 %{buildroot}/%{logfly_dir}
install -d -m 755 %{buildroot}/%{logfly_icon}
install -d -m 755 %{buildroot}/%{logfly_app}
install -d -m 755 %{buildroot}/%{udev_dir}

/usr/bin/cp -Ra * %{buildroot}/%{logfly_dir}/
cd -
install -m 644 %{SOURCE1} %{buildroot}/%{logfly_app}/%{_File1}
install -m 444 %{SOURCE2} %{buildroot}/%{logfly_icon}/%{_File2}
install -m 666 %{SOURCE3} %{buildroot}/%{udev_dir}/%{_File3}
cd -





#==============================================================================
# clean
#==============================================================================

%clean
%{__rm} -rf %{buildroot}

#==============================================================================
# Files métadonnées
#==============================================================================

%files
%defattr(-,root,root,-)
%{logfly_dir}
%{logfly_app}/%{_File1}
%{logfly_icon}/%{_File2}

%attr(646,root,root) %{udev_dir}/%{_File3}







%changelog
* Thu Apr 21 2022 Patrick Le Bas <plbs@free.fr>
- Initial version RPM for Logfly5 application
