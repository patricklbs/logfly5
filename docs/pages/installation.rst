=====================
Guide d'installation 
=====================

.. code-block:: bash

   sudo dnf install -y https://gitlab.com/patricklbs/logfly5/-/raw/master/package/Logfly-5-1.fc36.noarch.rpm

.. code-block:: bash

   wget https://gitlab.com/patricklbs/logfly5/-/raw/master/package/Logfly-5-1.fc36.noarch.rpm && sudo yum localinstall Logfly-5-1.fc36.noarch.rpm  -y



Distibutions de test
--------------------

L'installation du paquet a été testé sur les distributions suivantes :

- ``Fedora 35 Workstation``
- ``Fedora 36 Workstation``
- ``Ubuntu 22``
- ``Red Hat 8``
- ``Red Hat 9 Beta``
- ``Rocky Linux 8``

   

Notes additionnelles
--------------------

.. note::

* Le packet installe automatiquement le périphérique USB.
* La dépendance de GTK 2 est indispensable au fonctionnement de l'application et sera installé si non présente sur le système.

`Plus d'infos sur GTK <https://fr.wikipedia.org/wiki/GTK_(bo%C3%AEte_%C3%A0_outils)>`_


