Erreurs et solutions possibles
==============================

.. error:: Warning
        
        Warning: untrusted X11 forwarding setup failed: xauth key data not generated
        Last failed login: Mon May 16 15:21:13 CEST 2022 from 10.207.37.64 on ssh:notty

.. note:: Installer xauth ou ouvrir une session SSH avec **-Y** comme argument à la place de **X**

.. code-block:: bash

        yum install xauth -y
   
.. error:: /opt/Logfly5/Logfly5
        ./Logfly5: symbol lookup error: /tmp/sqlite-3.8.11.2-69882e4c-ba67-4f53-bd8c-5568330eabd8-libsqlitejdbc.so: undefined symbol: pthread_mutexattr_init

.. note:: Essayer d'installer "nautilus"


.. code-block:: bash

        yum install nautilus -y



.. error:: Cette erreur arrive lorsque il est fait un export DISPLAY et que l'application est appelée au travers de SSH par exemple.


.. code-block:: bash

        Graphics Device initialization failed for :  es2, sw
        Error initializing QuantumRenderer: no suitable pipeline found
        java.lang.RuntimeException: java.lang.RuntimeException: Error initializing QuantumRenderer: no suitable pipeline found
                at com.sun.javafx.tk.quantum.QuantumRenderer.getInstance(QuantumRenderer.java:280)
                at com.sun.javafx.tk.quantum.QuantumToolkit.init(QuantumToolkit.java:221)
                at com.sun.javafx.tk.Toolkit.getToolkit(Toolkit.java:248)
                at com.sun.javafx.application.PlatformImpl.startup(PlatformImpl.java:209)
                at com.sun.javafx.application.LauncherImpl.startToolkit(LauncherImpl.java:675)
                at com.sun.javafx.application.LauncherImpl.launchApplication1(LauncherImpl.java:695)
                at com.sun.javafx.application.LauncherImpl.lambda$launchApplication$154(LauncherImpl.java:182)
                at java.lang.Thread.run(Thread.java:748)
        Caused by: java.lang.RuntimeException: Error initializing QuantumRenderer: no suitable pipeline found
                at com.sun.javafx.tk.quantum.QuantumRenderer$PipelineRunnable.init(QuantumRenderer.java:94)
                at com.sun.javafx.tk.quantum.QuantumRenderer$PipelineRunnable.run(QuantumRenderer.java:124)
                ... 1 more
        Logfly5 Error invoking method.
        Logfly5 Failed to launch JVM

.. note:: Installer les paquets pour xorg

.. code-block:: bash

        yum install -y xorg-x11-server-utils xorg-x11-xauth xorg-x11-utils
