

.. image:: https://readthedocs.org/projects/logfly5/badge/?version=latest
    :target: https://logfly5.readthedocs.io/fr/latest/?badge=latest
    :alt: Documentation Status


.. sidebar:: A propos de logfly 5

    

    * **Website**: `https://logfly.org/ <https://logfly.org/>`_
    * **RPM source code**: `https://gitlab.com/patricklbs/logfly5/-/tree/master <https://gitlab.com/patricklbs/logfly5/-/tree/master>`_
    * **Source Code**: `https://github.com/giloutho/Logfly5 <https://github.com/giloutho/Logfly5>`_



Logfly pour GNU/Linux
=====================




.. image:: images/logo.png
    :alt: Logfly 5 gestion carnet de vols parapente
    :align: center
    :target: https://www.logfly.org/

Logfly 5 est un logiciel permettant la gestion de carnet de vols pour le parapente.
Ceci est une contribution pour l'installation de Logfly sur un système de type GNU/Linux "Red Hat Like" comme Fedora Core, CentOS, Rocky Linux.



.. toctree::
   :maxdepth: 2
   :caption: Sommaire :

   pages/installation
   pages/erreur





Liens
=====


* Home page                 : https://www.logfly.org/
* Source code               : https://gitlab.com/patricklbs/logfly5
* Lien de téléchargement    : https://gitlab.com/patricklbs/logfly5/-/raw/master/package/Logfly-5-1.fc36.noarch.rpm
* Contact                   : patricklbs[@]gmail.com